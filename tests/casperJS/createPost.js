//var casper = require('casper').create();

config.postForm = {
    '#post_title': 'REST APIs with Symfony2: The Right Way',
    '#post_url'  : 'http://williamdurand.fr/2012/08/02/rest-apis-with-symfony2-the-right-way/',
};

/****************************************
 *          Create a post               *
 ****************************************/

casper.test.begin('Create a post', 6, function(test){
    test.comment('... Loading ' + config.routes.post_index + '...');
    casper.start(config.routes.post_index);
    casper.then(function () {
        test.assertHttpStatus(200, config.siteName + ' is up');
    });

    //Clic on button "Add a new post"
    casper.then(function(){
        casper.capture('screenshots/createPost/1-postsList.png');
        test.assertExists('.add-post');
        this.click('.add-post');
    });
    test.comment('... Clicking on the button "Add a new post" ...');

    // Fill post form
    casper.waitForSelector("form#postForm", function() {
        test.comment('... Fill the post form ...');
        test.assertUrlMatch(this.getCurrentUrl(), config.routes.post_form_new);
        test.assertExists('form#postForm');
        this.fillSelectors('form#postForm', config.postForm, false);
    });

    casper.then(function(){
        casper.capture('screenshots/createPost/2-postForm.png');
        this.click('.btn-save');
    });

    // Assertions
    casper.waitForSelector(".btn-archive", function() {
        casper.capture('screenshots/createPost/3-postCreated.png');
        test.assertUrlMatch(this.getCurrentUrl(), config.routes.post_show);
        test.assertEvalEquals(function() {
            return __utils__.findOne('h2').textContent;
        }, config.postForm['#post_title']);
    });

    // Run all the tests defined above.
    casper.run(function(){
        test.done();
    });
});