//var casper = require('casper').create();

config.categoryForm = {
    '#category_title': 'PHP',
    '#category_description'  : 'Symfony, HHVM, Laravel...',
};

/****************************************
 *          Create a category           *
 ****************************************/

casper.test.begin('Create a category', 7, function(test){
    test.comment('... Loading ' + config.routes.post_index + '...');
    casper.start(config.routes.post_index);
    casper.then(function () {
        test.assertHttpStatus(200, config.siteName + ' is up');
    });

    //Clic on the link "Categories"
    casper.then(function(){
        casper.capture('screenshots/createCategory/1-postsList.png');
        test.assertExists('.link-categories');
        this.click('.link-categories');
    });
    test.comment('... Clicking on the link "Categories" ...');

    //Clic on button "Add a new category"
    casper.then(function(){
        casper.capture('screenshots/createCategory/2-categoriesList.png');
        test.assertExists('.add-category');
        this.click('.add-category');
    });
    test.comment('... Clicking on the button "Add a new category" ...');

    // Fill category form
    casper.waitForSelector("form#categoryForm", function() {
        test.comment('... Fill the category form ...');
        test.assertUrlMatch(this.getCurrentUrl(), config.routes.category_form_new);
        test.assertExists('form#categoryForm');
        this.fillSelectors('form#categoryForm', config.categoryForm, false);
    });

    casper.then(function(){
        casper.capture('screenshots/createCategory/3-categoryForm.png');
        this.click('.btn-save');
    });

    // Assertions
    casper.waitForSelector("h2.blog-post-title", function() {
        casper.capture('screenshots/createCategory/4-categoryCreated.png');
        test.assertUrlMatch(this.getCurrentUrl(), config.routes.post_show);
        test.assertEvalEquals(function() {
            return __utils__.findOne('h2.blog-post-title').textContent;
        }, config.categoryForm['#category_title']);
    });

    // Run all the tests defined above.
    casper.run(function(){
        test.done();
    });
});