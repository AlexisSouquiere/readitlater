<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Form\Type\CategoryType;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\Post;
use AppBundle\Form\Type\PostType;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * @Route("/api/categories")
 */
class CategoryController extends FOSRestController
{
    /**
     * @ApiDoc(
     *  description="Returns a collection of categories paginated",
     *  parameters={
     *      {"name"="page", "dataType"="integer", "required"=false, "description"="the number of the page"}
     *  },
     *  statusCodes={
     *      200="Returned when successful",
     *  }
     * )
     *
     * @Route("/", name="category_index")
     * @Route("/page/{page}", name="category_index_paginated", requirements={"page" : "\d+"}, defaults={"page" = 1})
     * @Method("GET")
     */
    public function getCategoriesAction($page)
    {
        $query = $this->getDoctrine()->getRepository('AppBundle:Category')->findAll();
        $paginator = $this->get('knp_paginator');
        $categories = $paginator->paginate($query, $page, Post::NUM_ITEMS);

        $view = $this->view($categories, Codes::HTTP_OK)
            ->setTemplate("AppBundle:category:index.html.twig")
            ->setTemplateVar('categories');

        return $this->handleView($view);
    }

    /**
     * Display a form to create a new category
     *
     * @Route("/new", name="category_form_new")
     * @Method("GET")
     */
    public function newCategoryFormAction(Request $request)
    {
        $form = $this->createForm(new CategoryType(), new Category());
        $form->handleRequest($request);

        $view = View::create()
            ->setData(array('form' => $form->createView()))
            ->setTemplate("AppBundle:category:new.html.twig");
        return $this->handleView($view);
    }

    /**
     * @ApiDoc(
     *  description="Create a new category",
     *  input="AppBundle\Entity\Category",
     *  statusCodes={
     *      201="Category created",
     *  }
     * )
     *
     * @Route("/", name="category_new")
     * @Method("POST")
     */
    public function newCategoryAction(Request $request)
    {
        return $this->processForm(new Category(), $request);
    }

    private function processForm(Category $category, Request $request)
    {
        $statusCode = ($category->getId() == null) ? Codes::HTTP_CREATED : Codes::HTTP_NO_CONTENT;

        $form = $this->createForm(new CategoryType(), $category, array('method' => $request->getMethod()));
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            if ($category->getId() == null) {
                $entityManager->persist($category);
            }
            $entityManager->flush();

            $this->addFlash('success', 'category.created_successfully');

            $view = View::create()
                ->setRouteParameters(array("id" => $category->getId()))
                ->setRoute('category_show')
                ->setStatusCode($statusCode);
            return $this->handleView($view);
        }

        $view = View::create()
            ->setData(array('form' => $form->createView(), 'category' => $category))
            ->setTemplate("AppBundle:category:new.html.twig");
        return $this->handleView($view);
    }

    /**
     * @ApiDoc(
     *  description="Update an existing category",
     *  input="AppBundle\Entity\Category",
     *  parameters={
     *      {"name"="id", "dataType"="string", "required"=true, "description"="category's uuid"},
     *  },
     *  statusCodes={
     *      201="Category created",
     *      204="Category not found",
     *  }
     * )
     *
     * @Route("/{id}", name="category_edit")
     * @Method("PUT")
     */
    public function editCategoryAction(Request $request, Category $category)
    {
        return $this->processForm($category, $request);
    }

    /**
     * @ApiDoc(
     *  description="Find a category by id",
     *  parameters={
     *      {"name"="id", "dataType"="string", "required"=true, "description"="category's uuid"},
     *  },
     *  statusCodes={
     *      200="Returned when successful",
     *      404={
     *           "Returned when the category is not found",
     *      }
     *  }
     * )
     *
     * @Route("/{id}", name="category_show")
     * @Method("GET")
     */
    public function getCategoryAction(Category $category)
    {
        $posts = $this->getDoctrine()->getRepository('AppBundle:Post')->findBy(array('category' => $category));

        $view = $this->view($category, Codes::HTTP_OK)
            ->setData(array('category' => $category, 'posts' => $posts))
            ->setTemplate("AppBundle:category:show.html.twig");

        return $this->handleView($view);
    }

    /**
     * @ApiDoc(
     *  description="Remove a category",
     *  parameters={
     *      {"name"="id", "dataType"="string", "required"=true, "description"="category's uuid"},
     *  },
     *  statusCodes={
     *      204="The category has been deleted",
     *      404={
     *           "Returned when the category is not found",
     *      }
     *  }
     * )
     *
     * @Route("/{id}", name="category_delete")
     * @Method("DELETE")
     */
    public function removeAction(Category $category)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($category);
        $em->flush();

        $view = View::create()
            ->setRoute('category_index_paginated')
            ->setStatusCode(Codes::HTTP_NO_CONTENT);
        return $this->handleView($view);
    }
}
