<?php
/**
 * Created by PhpStorm.
 * User: alexis
 * Date: 22/03/2016
 * Time: 09:23
 */

namespace AppBundle\Tests\Command;

use AppBundle\Utils\Crawler;
use Guzzle\Http\Message\Response;
use Guzzle\Plugin\Mock\MockPlugin;
use Guzzle\Http\Client as GuzzleClient;

class CrawlerTest extends \PHPUnit_Framework_TestCase
{
    private $html = <<<'HTML'
<!DOCTYPE html>
<html>
    <body>
        <article class="message">Hello World!</article>
        <p>Hello Crawler!</p>
    </body>
</html>
HTML;

    public function testExecute()
    {
        $crawler = $this->mockClient(new Response(200, null, $this->html));

        $content = $crawler->fetchContent('article', 'http://whatever');

        $this->assertEquals("Hello World!", $content);
    }

    protected function mockClient($response)
    {
        $mockClient = new GuzzleClient();

        $plugin = new MockPlugin();
        $plugin->addResponse($response);

        $mockClient->getEventDispatcher()->addSubscriber($plugin);

        return new Crawler($mockClient);
    }
}
