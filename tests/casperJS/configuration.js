/**
 * Created by Cedric on 24/03/2016.
 */
/****************************************
 *             Configuration            *
 ****************************************/

var config = {
    baseUrl: 'http://127.0.0.1:8000/api',
    siteName : 'RIL',
};

config.routes = {
    post_index: config.baseUrl + '/posts/page/1',
    post_form_new: config.baseUrl + '/posts/new',
    post_show: config.baseUrl + '/posts/*',
    category_index: config.baseUrl + '/categories/page/1',
    category_form_new: config.baseUrl + '/categories/new',
    category_show: config.baseUrl + '/categories/*',
}


/****************************************
 *             Casper Options           *
 ****************************************/

casper.options.viewportSize = { width: 1920, height: 1080 };