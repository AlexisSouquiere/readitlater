<?php

namespace AppBundle\Tests\Controller;

use AppBundle\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CategoryControllerTest extends WebTestCase
{
    private $em;
    private $baseUrl;

    public function testPostCategoryJson()
    {
        $client = static::createClient();
        $client->request('POST', $this->baseUrl.'/api/categories/',
            array(),
            array(),
            array(
                'HTTP_ACCEPT' => 'application/json',
                'CONTENT_TYPE' => 'application/json'
            ),
            json_encode(['category' => ['title' => 'test', 'description' => 'test description']])
        );

        $response = $client->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertContains('application/json', $response->headers->get('Content-Type'));
    }

    public function testPutCategoryJson()
    {
        // Arrange
        $category = $this->saveCategory();

        $category->setTitle("title");
        $category->setTitle("new Description");

        // Act
        $client = static::createClient();
        $client->request('PUT', $this->baseUrl.'/api/categories/'.$category->getId(),
            array(),
            array(),
            array(
                'HTTP_ACCEPT' => 'application/json',
                'CONTENT_TYPE' => 'application/json'
            ),
            json_encode(['category' => ['title' => $category->getTitle(), 'description' => $category->getDescription()]])
        );

        // Assert
        $freshCategory = $this->em->getRepository('AppBundle:Category')->find($category->getId());

        $response = $client->getResponse();
        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEquals($category->getId(), $freshCategory->getId());
        $this->assertEquals($category->getTitle(), $freshCategory->getTitle());
        $this->assertEquals($category->getDescription(), $freshCategory->getDescription());
    }

    private function saveCategory()
    {
        $category = new Category();
        $category->setTitle("test");
        $category->setDescription("description");

        $this->em->persist($category);
        $this->em->flush();

        return $category;
    }

    public function testDeleteCategory()
    {
        $category = $this->saveCategory();

        $client = static::createClient();
        $client->request('DELETE', $this->baseUrl.'/api/categories/'.$category->getId(),
            array(),
            array(),
            array(
                'HTTP_ACCEPT' => 'application/json'
            )
        );

        $response = $client->getResponse();

        $this->assertEquals(204, $response->getStatusCode());
    }

    public function testGetCategoryJson()
    {
        // Arrange
        $category = $this->saveCategory();

        // Act
        $client = static::createClient();
        $client->request('GET', $this->baseUrl.'/api/categories/'.$category->getId(),
            array(),
            array(),
            array(
                'HTTP_ACCEPT' => 'application/json'
            )
        );

        // Assert
        $response = $client->getResponse();
        $data = json_decode($response->getContent(), true);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('application/json', $response->headers->get('Content-Type'));
        $this->assertEquals($category->getId(), $data['category']['id']);
        $this->assertEquals($category->getTitle(), $data['category']['title']);
        $this->assertEquals($category->getDescription(), $data['category']['description']);
    }

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->baseUrl = static::$kernel->getContainer()->get('router')->getContext()->getBaseUrl();
    }
}
