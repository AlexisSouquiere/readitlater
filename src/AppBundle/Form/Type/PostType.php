<?php

namespace AppBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Created by PhpStorm.
 * User: Cedric
 * Date: 22/03/2016
 * Time: 15:09
 */
class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title');
        $builder->add('url');
        $builder->add('category', EntityType::class,
                    array(
                        'class' => 'AppBundle\Entity\Category',
                        'property' => 'title',
                        'query_builder' => function (EntityRepository $repository) {
                            return $repository->createQueryBuilder('c')
                                ->orderBy('c.title', 'ASC');
                        }
                    )
            );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'AppBundle\Entity\Post',
                'csrf_protection' => false)
        );
    }
    public function getName()
    {
        return 'post';
    }
}
