<?php

namespace AppBundle\Tests\Controller;

use AppBundle\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PostControllerTest extends WebTestCase
{
    private $em;
    private $baseUrl;

    public function testPostPostJson()
    {
        $client = static::createClient();
        $client->request('POST', $this->baseUrl.'/api/posts/',
            array(),
            array(),
            array(
                'HTTP_ACCEPT' => 'application/json',
                'CONTENT_TYPE' => 'application/json'
            ),
            json_encode(['post' => ['title' => 'test', 'url' => 'my_url']])
        );

        $response = $client->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertContains('application/json', $response->headers->get('Content-Type'));
    }

    public function testPutPostJson()
    {
        // Arrange
        $post = $this->savePost();

        $post->setTitle("title");
        $post->setUrl("new url");

        // Act
        $client = static::createClient();
        $client->request('PUT', $this->baseUrl.'/api/posts/'.$post->getId(),
            array(),
            array(),
            array(
                'HTTP_ACCEPT' => 'application/json',
                'CONTENT_TYPE' => 'application/json'
            ),
            json_encode(['post' => ['title' => $post->getTitle(), 'url' => $post->getUrl()]])
        );

        // Assert
        $freshPost = $this->em->getRepository('AppBundle:Post')->find($post->getId());

        $response = $client->getResponse();
        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEquals($post->getId(), $freshPost->getId());
        $this->assertEquals($post->getTitle(), $freshPost->getTitle());
        $this->assertEquals($post->getContent(), $freshPost->getContent());
    }

    private function savePost()
    {
        $post = new Post();
        $post->setTitle("test");
        $post->setContent("");
        $post->setUrl("my_url");
        $post->setArchived(false);
        $post->setPublishedAt(new \DateTime());

        $this->em->persist($post);
        $this->em->flush();

        return $post;
    }

    public function testDeletePost()
    {
        $post = $this->savePost();

        $client = static::createClient();
        $client->request('DELETE', $this->baseUrl.'/api/posts/'.$post->getId(),
            array(),
            array(),
            array(
                'HTTP_ACCEPT' => 'application/json'
            )
        );

        $response = $client->getResponse();

        $this->assertEquals(204, $response->getStatusCode());
    }

    public function testGetPostJson()
    {
        // Arrange
        $post = $this->savePost();

        // Act
        $client = static::createClient();
        $client->request('GET', $this->baseUrl.'/api/posts/'.$post->getId(),
            array(),
            array(),
            array(
                'HTTP_ACCEPT' => 'application/json'
            )
        );

        // Assert
        $response = $client->getResponse();
        $data = json_decode($response->getContent(), true);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('application/json', $response->headers->get('Content-Type'));
        $this->assertEquals($post->getId(), $data['id']);
        $this->assertEquals($post->getTitle(), $data['title']);
        $this->assertEquals($post->getContent(), $data['content']);
        $this->assertEquals($post->isArchived(), $data['archived']);
    }

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->baseUrl = static::$kernel->getContainer()->get('router')->getContext()->getBaseUrl();
    }
}
