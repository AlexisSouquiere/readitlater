<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\Post;
use AppBundle\Form\Type\PostType;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * @Route("/api/posts")
 */
class PostController extends FOSRestController
{
    /**
     * @ApiDoc(
     *  description="Returns a collection of posts paginated",
     *  parameters={
     *      {"name"="page", "dataType"="integer", "required"=false, "description"="the number of the page"}
     *  },
     *  statusCodes={
     *      200="Returned when successful",
     *  }
     * )
     *
     * @Route("/page/{page}", name="post_index_paginated", requirements={"page" : "\d+"}, defaults={"page" = 1})
     * @Route("/", name="post_index")
     * @Method("GET")
     */
    public function getPostsAction($page)
    {
        $query = $this->getDoctrine()->getRepository('AppBundle:Post')->queryLatest();
        $paginator = $this->get('knp_paginator');
        $posts = $paginator->paginate($query, $page, Post::NUM_ITEMS);

        $view = $this->view($posts, Codes::HTTP_OK)
            ->setTemplate('AppBundle:post:index.html.twig')
            ->setTemplateVar('posts');

        return $this->handleView($view);
    }

    /**
     * @Route("/archived", name="post_archived_index")
     * @Route("/archived/page/{page}", name="post_archived_index_paginated", requirements={"page" : "\d+"}, defaults={"page" = 1})
     * @Method("GET")
     */
    public function getPostsArchivedAction($page)
    {
        $query = $this->getDoctrine()->getRepository('AppBundle:Post')->getArchivedPosts();
        $paginator = $this->get('knp_paginator');
        $posts = $paginator->paginate($query, $page, Post::NUM_ITEMS);

        $view = $this->view($posts, Codes::HTTP_OK)
            ->setTemplate('AppBundle:post:index.html.twig')
            ->setTemplateVar('posts');

        return $this->handleView($view);
    }

    /**
     * Display the form to create a new post
     *
     * @Route("/new", name="post_form_new")
     * @Method("GET")
     */
    public function newPostFormAction()
    {
        $form = $this->createForm(new PostType(), new Post());
        $form->handleRequest($this->getRequest());

        $view = View::create()
            ->setData(array('form' => $form->createView()))
            ->setTemplate('AppBundle:post:new.html.twig');

        return $this->handleView($view);
    }

    /**
     * @ApiDoc(
     *  description="Create a new post",
     *  input="AppBundle\Entity\Post",
     *  statusCodes={
     *      201="Post created",
     *  }
     * )
     *
     * @Route("/", name="post_new")
     * @Method("POST")
     */
    public function newPostAction(Request $request)
    {
        return $this->processForm(new Post(), $request);
    }

    private function processForm(Post $post, Request $request)
    {
        $statusCode = ($post->getId() == null) ? Codes::HTTP_CREATED : Codes::HTTP_NO_CONTENT;

        $form = $this->createForm(new PostType(), $post, array('method' => $request->getMethod()));
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            if ($post->getId() == null) {
                $entityManager->persist($post);
                $post->setPublishedAt(new \DateTime());
                $post->setArchived(false);
            }
            $entityManager->flush();

            $this->addFlash('success', 'post.created_successfully');

            $view = View::create()
                ->setRouteParameters(array("id" => $post->getId()))
                ->setRoute('post_show')
                ->setStatusCode($statusCode);
            return $this->handleView($view);
        }

        $view = View::create()
            ->setData(array('form' => $form->createView(), 'post' => $post))
            ->setTemplate("AppBundle:post:new.html.twig");
        return $this->handleView($view);
    }

    /**
     * @ApiDoc(
     *  description="Find a post by id",
     *  parameters={
     *      {"name"="id", "dataType"="string", "required"=true, "description"="post's uuid"},
     *  },
     *  statusCodes={
     *      200="Returned when successful",
     *      404={
     *           "Returned when the post is not found",
     *      }
     *  }
     * )
     *
     * @Route("/{id}", name="post_show")
     * @Method("GET")
     */
    public function getPostAction(Post $post)
    {
        $view = $this->view($post, Codes::HTTP_OK)
            ->setTemplate('AppBundle:post:show.html.twig')
            ->setTemplateVar('post');

        return $this->handleView($view);
    }

    /**
     * @ApiDoc(
     *  description="Archive a post",
     *  input="AppBundle\Entity\Post",
     *  parameters={
     *      {"name"="id", "dataType"="string", "required"=true, "description"="post's uuid"},
     *  },
     *  statusCodes={
     *      204="No content",
     *  }
     * )
     *
     * @Route("/{id}/archived", name="post_archive")
     * @Method("PUT")
     */
    public function putPostArchiveAction(Post $post)
    {
        $post->setArchived(true);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($post);
        $entityManager->flush();

        $view = View::create()
            ->setRoute('post_index_paginated')
            ->setStatusCode(Codes::HTTP_NO_CONTENT);
        return $this->handleView($view);
    }

    /**
     * @ApiDoc(
     *  description="Update an existing post",
     *  input="AppBundle\Entity\Post",
     *  parameters={
     *      {"name"="id", "dataType"="string", "required"=true, "description"="post's uuid"},
     *  },
     *  statusCodes={
     *      201="Post created",
     *      204="Post not found",
     *  }
     * )
     *
     * @Route("/{id}", name="post_edit")
     * @Method("PUT")
     */
    public function editPostAction(Request $request, Post $post)
    {
        return $this->processForm($post, $request);
    }
    /**
     * @ApiDoc(
     *  description="Remove a post",
     *  parameters={
     *      {"name"="id", "dataType"="string", "required"=true, "description"="post's uuid"},
     *  },
     *  statusCodes={
     *      204="The post has been deleted",
     *      404={
     *           "Returned when the post is not found",
     *      }
     *  }
     * )
     *
     * @Route("/{id}", name="post_delete")
     * @Method("DELETE")
     */
    public function removeAction(Post $post)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();

        $view = View::create()
            ->setRoute('post_index_paginated')
            ->setStatusCode(Codes::HTTP_NO_CONTENT);
        return $this->handleView($view);
    }
}
