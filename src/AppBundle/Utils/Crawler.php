<?php
/**
 * Created by PhpStorm.
 * User: alexis
 * Date: 22/03/2016
 * Time: 11:29
 */

namespace AppBundle\Utils;

use Goutte\Client;

class Crawler
{
    private $client;

    public function __construct($client = null)
    {
        $this->client = new Client();

        if ($client != null) {
            $this->client->setClient($client);
        }
    }

    public function fetchContent($selector, $uri)
    {
        $crawler = $this->client->request('GET', $uri);

        // Fetch HTML content
        $content = $crawler->filter($selector)->html();

        return $content;
    }
}
