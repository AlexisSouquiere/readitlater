Read It Later
===

A Symfony project created on February 10, 2016, 10:39 am.

Authors : Cédric GAGNEVIN & Alexis SOUQUIERE

Installation
------

* Configure the database host and port into app/config/parameters.yml
* Create dev and test database by running the following commands for each environment:
`php app/console doctrine:database:create --env=<dev/test>` and `php app/console doctrine:schema:creation --env<dev/test>`
* Install the assets using `php app/console assets:install`
* Launch the embedded server with `php app/console server:start`
* Start the worker by adding `* * * * * php /<path_to_the_project>/app/console content:fetch > /dev/null` into the crontab
* Install the chrome extension "ril-plugin.crx" : drag and drop the file into the extension window of Google Chrome.

Features
-----

* Symfony 2.8 project
* Doctrine ORM
* Twig template engine
* Rest API serving HTML or JSON with the HTTP verb and the right status code (200 ok, 201 created, 204 no content, etc.)
* Worker that retrieve periodically the content of the posts. Use the Symfony DOM Crawler and fabpot/goutte. Get the page content using the tag "<article>".
* Chrome extension to easily add a post
* Tested using CasperJS (UI) and PHPUnit (REST API + worker)
* API Documentation using Nelmio API doc bundle (location: /api/doc)
* i18n ready. Only english provided.
* Coding Standards : PSR-1 and PSR-2.
* User can : create category, create post manually/using the chrome extension, archive a post, delete a post or a category

Tests
-----

* Run phpunit
* Run CasperJS tests with the command `path/to/project/tests/casperJS> 	casperjs test createPost.js --includes=configuration.js`
* Send a curl command like `curl -v -X "POST" -H "Content-Type: application/json" -H "Accept: application/json" -d '{"category":{"title":"My category","description":"My description"}}' http://127.0.0.1:8000/api/categories/`