<?php
/**
 * Created by PhpStorm.
 * User: alexis
 * Date: 10/02/2016
 * Time: 11:34
 */

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Goutte\Client;

class FetchContentCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('content:fetch');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = $this->getContainer()->get('logger');

        $em = $this->getContainer()->get('doctrine')->getManager();
        $posts = $em->getRepository('AppBundle:Post')->getPostsToDownload();
        $crawler = $this->getContainer()->get('app.crawler');

        $output->writeln("Start fetching content");

        foreach ($posts as $post) {
            $output->writeln('Fetching post content at URL '.$post->getUrl());

            // Fetch the content
            $content = $crawler->fetchContent('article', $post->getUrl());

            // Add the content to the post
            $post->setContent($content);

            $output->writeln('Post content successfully fetched');
        }

        // Save changes
        $em->flush();

        $output->writeln("End fetching content");
    }
}
