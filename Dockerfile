FROM linode/lamp

RUN apt-get update && apt-get install -y php5-mysql
RUN service apache2 start
RUN service mysql start
EXPOSE 3306